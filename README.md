# Redmine Docker

This is my small enhancement to the official Redmine container. The Dockerfile is quite short and uses `FROM redmine:3.4`

## Main changes

* Add environment variables for configuring SMTP settings for sending mail
* Add rufus-scheduler for cron-like polling of IMAP folders
* Add environment variables for configuring IMAP/Polling settings.
* Add confd (https://github.com/kelseyhightower/confd/) to run prior to redmine to configure SMTP and IMAP using templates.
* Various plugins added which I find useful:
 * Knowledgebase
 * Autorespond
 * Impersonate
 * Mentions
 * My_Page
 * Project Specific Email Sender
 * Redmine Additionals (formerly Tweaks)
 * Redmine Image Clipboard Paste
 * Redmine Custom CSS
 * Redmine Draw.io
 * Redmine Messenger
 * Redmine Smile Toggle Sidebar


Below are the environement variables which can be set (in addition to the ones form the main redmine image):

```
SMTP_STARTTLS =  true or false
SMTP_SERVER = hostname of the SMTP server
SMTP_PORT = SMTP port
SMTP_DOMAIN = SMTP domain (usually same as SERVER)
SMTP_AUTH_METHOD = auth method, usually "plain"
SMTP_USERNAME = SMTP username
SMTP_PASSWORD = SMTP password
IMAP_HOST = IMAP host for mailbox polling
IMAP_PORT = IMAP host
IMAP_SSL = true or false
IMAP_USERNAME = IMAP username
IMAP_PASSWORD = IMAP password
IMAP_DEFAULT_PROJECT = Default project you want to place incoming emails in if not overridden in the email or subaddress
IMAP_DEFAULT_TRACKER = default tracker to use for issue created
IMAP_NO_ACCOUNT_NOTICE = no_account_notice setting, true or false
IMAP_DEFAULT_GROUP = If unknwon_user_action = create, which redmine group should the user be created in
IMAP_ALLOW_OVERRIDE = redmine allow_override setting (e.g. all, none, project, tracker, etc)
IMAP_PROJECT_FROM _SUBADDRESS = Redmine project_from_subaddress setting.
IMAP_UNKNOWN_USER_ACTION = Redmin eunknown_user_action setting
IMAP_NO_CHECK_PERMISSION = Redmine no_check_permission setting
```

Please see http://www.redmine.org/projects/redmine/wiki/RedmineReceivingEmails documentation for details on the above settings.

## Customising 

To build your own images, first clone this repo, then cd into it and run

```
 git submodule init
 git submodule update
 docker build -t myredmine .
```


