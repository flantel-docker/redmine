FROM redmine:3.4

ENV TZ=Europe/Dublin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /usr/src/redmine
COPY configuration.yml.toml /etc/confd/conf.d/
COPY configuration.yml.tmpl /etc/confd/templates/
COPY getmail.rb.toml /etc/confd/conf.d/
COPY getmail.rb.tmpl /etc/confd/templates/
COPY run.sh /
COPY plugins plugins/
COPY themes public/themes/
COPY images public/images/
COPY confd /usr/bin/confd
RUN gem install rufus-scheduler && \
    echo "gem 'rufus-scheduler'" >> Gemfile && \
    bundle install --without development test
CMD ["/run.sh"]

